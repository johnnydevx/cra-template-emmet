import AppContext, { reducer, getLocal } from 'App/context';
import { Route, Switch, useLocation } from 'wouter';
import { useReducer, useRef } from 'react';
import ROUTES from 'routes';
import Page404 from 'App/common/Page404';
import useEmmet from 'emmet';
import config from 'emmet.config';
import 'index.css';

function App() {
  const [location] = useLocation();
  const [state, set] = useReducer(reducer, {
    theme: getLocal('theme') || 0,
  });

  const base = useRef(null);

  useEmmet(base, config, state.theme, [state.theme, location]);

  return (
    <AppContext.Provider value={{ state, set }}>
      <div ref={base} data-emmet="await w h fx1 bg$root c$t df ovh">
        <Switch>
          {ROUTES.map(route => (
            <Route
              key={route.path}
              path={route.path}
              component={route.component}
            />
          ))}
          <Route>
            <Page404 />
          </Route>
        </Switch>
      </div>
    </AppContext.Provider>
  );
}

export default App;
