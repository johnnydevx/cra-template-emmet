const en = {
  translation: {
    // Those are default strings provided in t("English string")
    // we don't have to provide them here
  },
};

export default en;
