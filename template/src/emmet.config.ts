import COLORS from 'emmet/colors';

const config = {
  variables: {
    'root': '#FFFFFF',
    'font-default': '',
    'default-border-color': '#999999',

    'primary': '#00B294',
    'primary-text': '#fff',
    'accent': '#F9E441',
    'accent-text': '#323639',

    's': COLORS.gray900,
    'hover': COLORS.gray800,
    'active': COLORS.gray700,
  },
  shortcuts: {
    section: 'w py128 px32 df fxdc aic',
    label: 'c$primary fw7 fz20',
    header: 'fz48 fw7 mb64',
    row: 'df aic',
  },
  custom: {
    'gradient-faq': 'background: linear-gradient(to bottom, #EFF4FB, #fff);',
    't-clip':
      'background-clip: text; -webkit-background-clip: text; color: transparent;',
    'shadow': 'box-shadow: 0 2px 16px 1px #0002;',
    'vh': `
      clip: rect(0 0 0 0);
      clip-path: inset(50%);
      height: 1px;
      overflow: hidden;
      position: absolute;
      white-space: nowrap;
      width: 1px;
  `,
    'tol': `
      white-space: nowrap;
      overflow: hidden;
      display: block;
      text-overflow: ellipsis;
  `,
  },
};

export default config;
