import { createContext } from 'react';

interface AppState {
  theme: number;
}

interface AppContextType {
  state: AppState;
  set: any;
}

const AppContext = createContext<AppContextType>(null);

const ID = 'AppState';

type ContextReducer = (state, object) => any;

export const reducer: ContextReducer = (state, object) => {
  const PERSIST_KEYS = ['theme'];

  for (const key in object) {
    if (PERSIST_KEYS.includes(key)) {
      localStorage.setItem(`${ID}-${key}`, JSON.stringify(object[key]));
    }
  }

  return { ...state, ...object };
};

export const getLocal = key => {
  return JSON.parse(localStorage.getItem(`${ID}-${key}`)) || null;
};

export default AppContext;
