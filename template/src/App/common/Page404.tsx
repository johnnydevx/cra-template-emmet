const Page404 = () => (
  <div data-emmet="W H bg$primary df aic jcc fz48 fw7">404 Not Found</div>
);

export default Page404;
