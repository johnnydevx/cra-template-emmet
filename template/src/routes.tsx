import Home from 'App/Home';

const ROUTES: any = [{ name: 'Home', path: '/', component: Home }];

export default ROUTES;
