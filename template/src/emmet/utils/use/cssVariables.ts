import { useEffect } from 'react';

/**
 *
 * @example
 * useCssVariables({
 *   'red400': '#123123',
 *   'red500': '#234234',
 * })
 */
const useCssVariables = obj => {
  useEffect(() => {
    for (const key in obj)
      document.documentElement.style.setProperty(`--${key}`, obj[key]);
  }, [obj]);
};

export default useCssVariables;
