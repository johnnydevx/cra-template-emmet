const log = (text, ...args) => console.log(`🧞‍♂️ ${text}`, ...args);

export default log;
