const prefixObjectKeys = (obj, prefix) => {
  return Object.fromEntries(
    Object.entries(obj).map(([key, val]) => {
      return [prefix + key, val];
    })
  );
};

export default prefixObjectKeys;
