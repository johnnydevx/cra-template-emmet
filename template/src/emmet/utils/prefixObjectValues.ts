const prefixValues = (prefix, obj) => {
  return Object.fromEntries(
    Object.entries(obj).map(([key, value]) => {
      return [key, prefix + value];
    })
  );
};

export default prefixValues;
