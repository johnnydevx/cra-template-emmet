const createStyleSheet = id => {
  const styleSheet = document.createElement('style');
  styleSheet.setAttribute('id', id);
  styleSheet.innerHTML = '';
  document.querySelector('head').appendChild(styleSheet);
};

export default createStyleSheet;
