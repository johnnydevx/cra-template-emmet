# EmmetJSX
Runtime-generated dynamic stylesheets, based on [Emmet CSS abbreviations](https://docs.emmet.io/cheat-sheet/).

## Setup
```tsx
  const base = useRef(null);

  useEmmet(base, config, themeIndex, [themeIndex, location]);

  return (
    <div ref={base}>
      ...
    </div>
  )
```