/**
 * @param base Base element reference
 *
 * @returns
 * Array containing all elements we are going to process,
 * that is the base element that ref is passed to, and all its
 * children containing `data-emmet` attribute.
 *
 */
const getScope = (base: React.MutableRefObject<any>): HTMLElement[] => {
  return [base.current, ...base.current.querySelectorAll('[data-emmet]')];
};

export default getScope;
