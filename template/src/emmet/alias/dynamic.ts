const isNumber = str => /^-?\d+$/.test(str);
const isHexColor = str =>
  /^([0-9A-F]){3}|([0-9A-F]){4}|([0-9A-F]){6}|([0-9A-F]){8}$/i.test(str);

const DYNAMIC = {
  // Flex
  fx: v => isNumber(v) && `flex: ${v}`,
  fb: v => isNumber(v) && `flex-basis: ${v}`,
  fxs: v => isNumber(v) && `flex-shrink: ${v}`,
  g: v => isNumber(v) && `gap: ${v}px`,

  w: v =>
    isNumber(v) ? `width: ${v}px` : v.slice(-1) === '%' ? `width: ${v}` : '',
  h: v =>
    isNumber(v) ? `height: ${v}px` : v.slice(-1) === '%' ? `height: ${v}` : '',

  mh: v => `min-height: ${v}px`,
  mw: v => `min-width: ${v}px`,

  maw: v => isNumber(v) && `max-width: ${v}px`,
  o: v => isNumber(v) && `opacity: 0.${v}`,
  bgc: v => {
    if (v.startsWith('$')) return `background-color: var(--${v.slice(1)})`;
    if (isHexColor(v)) return `background-color: #${v}`;
  },
  bg: v => {
    if (v.startsWith('$')) return `background: var(--${v.slice(1)})`;
    if (isHexColor(v)) return `background: #${v}`;
  },
  c: v => {
    if (v.startsWith('$')) return `color: var(--${v.slice(1)})`;
    if (isHexColor(v)) return `color: #${v}`;
  },
  s: v => {
    if (isNumber(v)) return `width: ${v}px; height: ${v}px`;
  },

  pt: v => `padding-top: ${v}px`,
  pr: v => `padding-right: ${v}px`,
  pb: v => `padding-bottom: ${v}px`,
  pl: v => `padding-left: ${v}px`,

  px: value => {
    if (isNumber(value))
      return `padding-left: ${value}px; padding-right: ${value}px`;
  },
  py: value => {
    if (isNumber(value))
      return `padding-top: ${value}px; padding-bottom: ${value}px`;
  },
  p: value => isNumber(value) && `padding: ${value}px`,

  // margin
  m: value => isNumber(value) && `margin: ${value}px`,
  mt: value => isNumber(value) && `margin-top: ${value}px`,
  mr: value => isNumber(value) && `margin-right: ${value}px`,
  mb: value => isNumber(value) && `margin-bottom: ${value}px`,
  ml: value => isNumber(value) && `margin-left: ${value}px`,
  mx: value => {
    if (isNumber(value))
      return `margin-left: ${value}px; margin-right: ${value}px`;
  },
  my: value => {
    if (isNumber(value))
      return `margin-top: ${value}px; margin-bottom: ${value}px`;
  },

  // Font
  fz: value => isNumber(value) && `font-size: ${value}px`,
  ff: value => `font-family: ${value}`,
  fw: v => isNumber(v) && v.length === 1 && `font-weight: ${v}00`,
  lh: v => isNumber(v) && `line-height: ${v}`,

  ls: value => isNumber(value) && `letter-spacing: ${value}px`,
  t: value => `top: ${value}`,
  l: value => `left: ${value}`,
  r: value => `right: ${value}`,
  b: value => `bottom: ${value}`,
  z: v => isNumber(v) && `z-index: ${v}`,

  // border
  bdr: v => {
    if (v.split('-').length === 2)
      return `border-right: ${v.split('-')[0]}px solid ${v.split('-')[1]}`;
  },
  bdb: v => {
    if (v.split('-').length === 2)
      return `border-bottom: ${v.split('-')[0]}px solid ${v.split('-')[1]}`;
  },

  /* Border
  bd: v => {
    return `border: 1px solid #${v}`;
  },*/

  // border-radius
  bdrs: v => isNumber(v) && `border-radius: ${v}px`,
  bdtrrs: v => isNumber(v) && `border-top-right-radius: ${v}px`,
  bdtlrs: v => isNumber(v) && `border-top-left-radius: ${v}px`,
  bdbrrs: v => isNumber(v) && `border-bottom-right-radius: ${v}px`,
  bdblrs: v => isNumber(v) && `border-bottom-left-radius: ${v}px`,

  // Grid
  gtc: v => isNumber(v) && `grid-template-columns: repeat(${v}, 1fr)`,
  gy: v => isNumber(v) && `column-gap: ${v}px`,

  // Background
  bgi: v => `background-image: url('/assets/${v}')`,
  bgs: v => `background-size: ${v}`,
  bgpx: v => `background-position-x: ${v}`,
  bgpy: v => `background-position-y: ${v}`,

  // Transform, e.g. trf:r4,ty-80 = transform: rotate(4deg) translateY(-80px);
  trf: v => {
    const TRANSFORMS: Array<[string, Function]> = [
      ['tx', v => `translateX(${v})`],
      ['ty', v => `translateY(${v})`],
      ['sx', v => `scaleX(${v})`],
      ['sy', v => `scaleY(${v})`],
      ['r', v => `rotate(${v}deg)`],
      ['s', v => `scale(${v})`],
    ];

    let transform = 'transform: ';

    v.slice(1)
      .split(',')
      .forEach(rule => {
        for (const [prefix, compile] of TRANSFORMS) {
          if (rule.startsWith(prefix)) {
            transform += compile(rule.slice(prefix.length)) + ' ';
            break;
          }
        }
      });

    return transform;
  },

  // Transition
  trs: v => isNumber(v) && `transition: ${v}ms`,

  // Filter
  ftbl: v => `filter: blur(${v}px)`,
  ftbr: v => `filter: brightness(${v})`,
};

export default DYNAMIC;
