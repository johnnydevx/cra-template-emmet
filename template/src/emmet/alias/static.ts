import prefixValues from 'emmet/utils/prefixObjectValues';

const STATIC = {
  t: 'top: 0',
  r: 'right: 0',
  b: 'bottom: 0',
  l: 'left: 0',

  w: 'width: 100%',
  W: 'width: 100vw',
  h: 'height: 100%',
  H: 'height: 100vh',
  wa: 'width: auto',
  ha: 'height: auto',
  o: 'opacity: 1',

  bdrs: 'border-radius: 50%',
  bdn: 'border: none',

  // margin
  mt: 'margin-top: auto',
  mr: 'margin-right: auto',
  mb: 'margin-bottom: auto',
  ml: 'margin-left: auto',
  mx: 'margin-left: auto; margin-right: auto;', // composed, imp. wont work

  fxdr: 'flex-direction: row',
  fxdrr: 'flex-direction: row-reverse',
  fxdc: 'flex-direction: column',
  fxdcr: 'flex-direction: column-reverse',
  fxs0: 'flex-shrink:0',

  ...prefixValues('justify-content: ', {
    jcs: 'start',
    jcfs: 'flex-start',
    jcc: 'center',
    jce: 'end',
    jcfe: 'flex-end',
    jcsb: 'space-between',
    jcsa: 'space-around',
    jcse: 'space-evenly',
  }),

  ais: 'align-items: start',
  aic: 'align-items: center',
  aie: 'align-items: end',
  ait: 'align-items: stretch',

  wwbw: 'word-wrap: break-word',
  ttu: 'text-transform: uppercase',
  usn: 'user-select: none',
  tac: 'text-align: center',

  ...prefixValues('cursor: ', {
    cud: 'default',
    cup: 'pointer',
    cug: 'grab',
  }),

  ...prefixValues('position: ', {
    pos: 'static',
    por: 'relative',
    poa: 'absolute',
    pof: 'fixed',
    pot: 'sticky',
  }),

  ...prefixValues('display: ', {
    db: 'block',
    di: 'inline',
    dib: 'inline-block',
    df: 'flex',
    dif: 'inline-flex',
    dg: 'grid',
    dig: 'inline-grid',
    dn: 'none',
  }),

  ...prefixValues('overflow: ', {
    ovv: 'visible',
    ova: 'auto',
    ovh: 'hidden',
    ovs: 'scroll',
  }),

  ...prefixValues('overflow-x: ', {
    ovxv: 'visible',
    ovxa: 'auto',
    ovxh: 'hidden',
    ovxs: 'scroll',
  }),

  ...prefixValues('overflow-y: ', {
    ovyv: 'visible',
    ovya: 'auto',
    ovyh: 'hidden',
    ovys: 'scroll',
  }),

  // background-size
  bgscv: 'background-size: cover',
  bgsct: 'background-size: contain',

  // background-position
  bgpc: 'background-position: center',
  bgpt: 'background-position: top',
  bgpl: 'background-position: left',
  bgpr: 'background-position: right',
  bgpb: 'background-position: bottom',

  // background-repeat
  bgrnr: 'background-repeat: no-repeat',

  pen: 'pointer-events: none',

  togt: 'transform-origin: top',
};

export default STATIC;
