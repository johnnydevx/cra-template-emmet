import { useEffect } from 'react';
import STATIC from './alias/static';
import DYNAMIC from './alias/dynamic';
import COLORS from './colors';
import useCssVariables from 'emmet/utils/use/cssVariables';
import createStyleSheet from 'emmet/utils/createStyleSheet';
import log from './utils/log';
import getScope from 'emmet/core/getScope';
import 'emmet/css/core.css';

const compileStyle = (className, custom = {}, forceImportant = false) => {
  const important = className.slice(-1) === '!';
  const cn = important ? className.slice(0, -1) : className;

  const postfix = important || forceImportant ? ' !important;' : ';';

  if (custom[cn]) return custom[cn];
  if (STATIC[cn]) return STATIC[cn] + postfix;

  const dynamics = Object.keys(DYNAMIC).map(k => [k, DYNAMIC[k]]);
  dynamics.sort((a, b) => b[0].length - a[0].length);

  for (const [key, compile] of dynamics) {
    if (cn.startsWith(key)) {
      const style = compile(cn.slice(key.length));
      if (style) return style + postfix;
    }
  }

  return '[CANNOT-COMPILE]';
};

/**
 *
 * @param base Base element reference
 * @param config
 * @param theme
 * @param deps
 */
const useEmmet = (
  base: React.MutableRefObject<any>,
  config: any,
  theme: any,
  deps: any
) => {
  const themedVariables = Object.fromEntries(
    Object.entries(config.variables).map(([key, value]) => {
      return [key, Array.isArray(value) ? value[theme] : value];
    })
  );

  useCssVariables({
    ...COLORS,
    ...themedVariables,
  });

  useEffect(() => {
    createStyleSheet('emmetjsx-layer-base');
    createStyleSheet('emmetjsx-layer-respo');
  }, []);

  useEffect(() => {
    if (!base.current) return;

    const start = performance.now();
    const scope = getScope(base);

    let iid = 1;
    let baseStyleSheet = '';
    let respo = [];

    scope.forEach(element => {
      element.setAttribute('data-id', (++iid).toString());

      const emmet = element.getAttribute('data-emmet').split(' ');
      const base = emmet.filter(s => !s.startsWith('<'));
      const respos = emmet.filter(s => s.startsWith('<'));

      let elementStyle = emmet.includes('await') ? `visibility: visible;` : '';

      const compileStyles = piece => {
        if (config.shortcuts[piece]) {
          return config.shortcuts[piece].split(' ').forEach(compileStyles);
        }

        let style = compileStyle(piece, config.custom, false);

        if (style === '[CANNOT-COMPILE]') {
          let cn = piece.trim();
          if (cn.length > 0) {
            element.classList.add(piece);
          }
        } else {
          elementStyle += style;
        }
      };

      base.forEach(compileStyles);

      baseStyleSheet += `[data-id="${iid}"] { ${elementStyle} }`;

      respos.forEach(piece => {
        const [breakpoint, rule] = piece.slice(1).split(':');
        respo.push([iid, breakpoint, rule]);
        return;
      });
    });

    document.querySelector('#emmetjsx-layer-base').innerHTML = baseStyleSheet;

    let respoStyleSheetContent = '';

    respo.forEach(([id, breakpoint, rules]) => {
      rules.split(',').forEach(rule => {
        const style = compileStyle(rule, config.custom, true);

        respoStyleSheetContent += `
            @media (max-width: ${breakpoint}px) {
              [data-id="${id}"] { ${style} }      
            }
          `;
      });
    });

    document.querySelector('#emmetjsx-layer-respo').innerHTML =
      respoStyleSheetContent;

    const compilationTime = performance.now() - start;

    log(
      `Compiled ${scope.length} elements in ${compilationTime
        .toString()
        .slice(0, 4)}ms`
    );
  }, [base, config, config.custom, theme, ...deps]);
};

export default useEmmet;
